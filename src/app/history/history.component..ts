import { Component, OnInit, ViewChild } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { MatPaginator, MatSort } from '@angular/material';
import { HistoryDataSource } from './history.datasource';

@Component({
  selector: 'app-history',
  templateUrl: './history.component.html',
  styleUrls: ['./history.component.css']
})
export class HistoryComponent implements OnInit {
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  dataSource: HistoryDataSource;

  displayedColumns = ['IP', 'image', 'clickPos', 'slot', 'duration'];

  constructor(private http: HttpClient) {}

  ngOnInit() {
    this.dataSource = new HistoryDataSource(this.paginator, this.sort, this.http);
  }
}
