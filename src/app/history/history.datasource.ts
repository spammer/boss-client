import { DataSource } from '@angular/cdk/table';
import { MatPaginator, MatSort } from '@angular/material';
import { HttpClient } from '@angular/common/http';
import { merge, Observable, of } from 'rxjs';
import { catchError, map, switchMap, startWith } from 'rxjs/operators';
import { environment } from '../../environments/environment';

export class HistoryItem {
  _id: string;

  constructor(o: any) {
    Object.assign(this, o);
  }
}
export class HistoryAPI {
  items: HistoryItem[];
  total: number;
}

export class HistoryDataSource extends DataSource<HistoryItem> {
  isLoadingResults = true;
  total: number;

  constructor(private paginator: MatPaginator, private sort: MatSort, private http: HttpClient) {
    super();
  }

  connect(): Observable<HistoryItem[]> {
    this.sort.sortChange.subscribe(() => this.paginator.pageIndex = 0);
    return merge(this.sort.sortChange, this.paginator.page).pipe(
      startWith({}),
      switchMap(() => {
        this.isLoadingResults = true;
        return this.http.get<HistoryAPI>([
          `${environment.apiUrl}/boss/history?`,
          `sort=${this.sort.active}&`,
          `order=${this.sort.direction}&`,
          `page=${this.paginator.pageIndex + 1}`
        ].join(''));
      }),
      map(data => {
        this.isLoadingResults = false;
        this.total = data.total;
        return data.items.map(o => new HistoryItem(o));
      }),
      catchError(() => {
        this.isLoadingResults = false;
        return of([]);
      })
    );
  }

  disconnect() {}
}
