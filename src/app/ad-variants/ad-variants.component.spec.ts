
import { fakeAsync, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdVariantsComponent } from './ad-variants.component';

describe('AdVariantsComponent', () => {
  let component: AdVariantsComponent;
  let fixture: ComponentFixture<AdVariantsComponent>;

  beforeEach(fakeAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ AdVariantsComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(AdVariantsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should compile', () => {
    expect(component).toBeTruthy();
  });
});
