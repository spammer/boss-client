import { DataSource } from '@angular/cdk/collections';
import { MatPaginator, MatSort } from '@angular/material';
import { debounceTime, map, startWith, switchMap, catchError } from 'rxjs/operators';
import { Observable, fromEvent, of as observableOf, merge } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { ElementRef } from '@angular/core';
import { environment } from '../../environments/environment';

interface Selector {
  selector: string;
  length: number;
  comment: string;
}
export class AdVariantsItem {
  _id: string;
  name: string;
  advertiser: string;
  selPatterns: Array<Selector>;
  clicks: {
    sum: number;
    positions: string;
  };
  get selPatternsTable(): string {
    return this.selPatterns
      .map(s => `${s.selector}; <b>#${s.length}</b><i>${s.comment || ''}</i>`)
      .join('<br>');
  }
  get selPatternsTxt(): string {
    return JSON.stringify(this.selPatterns, null, 2);
  }
  set selPatternsTxt(txt) {
    this.selPatterns = JSON.parse(txt);
  }
  image: string;
  code: string;
  adSize: string;
  createdAt: Date;
  updatedAt: Date;

  constructor(o: any) {
    Object.assign(this, o);
    if (!o.clicks) {
      this.clicks = {
        sum: 0,
        positions: ''
      };
    }
    // TODO: test if clicks is empty in new db.adpatterns
  }
}

interface API {
  items: any[];
  total: number;
}

export class AdVariantsAPI implements API {
  items: AdVariantsItem[];
  total: number;
}

abstract class BossDataSource<I, A extends API> extends DataSource<I> {
  isLoadingResults = true;
  total: number;
  items: I[];

  constructor(private filter: ElementRef, private paginator: MatPaginator, private sort: MatSort, private http: HttpClient) {
    super();
  }

  abstract transformItem(o: I);

  /**
   * Connect this data source to the table. The table will only update when
   * the returned stream emits new items.
   * @returns A stream of the items to be rendered.
   */
  connect(): Observable<I[]> {
    this.sort.sortChange.subscribe(() => this.paginator.pageIndex = 0);
    return merge(fromEvent(this.filter.nativeElement, 'keyup'), this.sort.sortChange, this.paginator.page).pipe(
      debounceTime(300),
      startWith({}),
      switchMap(() => {
        this.isLoadingResults = true;
        return this.http.get<A>([
          `${environment.apiUrl}/boss/ad-variants?`,
          `filter=${this.filter.nativeElement.value}&`,
          `sort=${this.sort.active || ''}&`,
          `order=${this.sort.direction}&`,
          `page=${this.paginator.pageIndex}&`,
          `pageSize=${this.paginator.pageSize}`
        ].join(''));
      }),
      map(data => {
        // Flip flag to show that loading has finished.
        this.isLoadingResults = false;
        this.items = data.items.map(o => this.transformItem(o));
        this.total = data.total;
        return this.items;
      }),
      catchError((err) => {
        this.isLoadingResults = false;
        console.error(err);
        return observableOf([]);
      })
    );
  }

  /**
   *  Called when the table is being destroyed. Use this function, to clean up
   * any open connections or free any held resources that were set up during connect.
   */
  disconnect() {}
}

export class AdVariantsDataSource extends BossDataSource<AdVariantsItem, AdVariantsAPI> {
  transformItem (o) {
    return new AdVariantsItem(o);
  }
}
