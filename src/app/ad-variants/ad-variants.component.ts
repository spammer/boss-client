import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { MatDialog, MatPaginator, MatSort } from '@angular/material';
import { AdVariantsDataSource, AdVariantsItem } from './ad-variants-datasource';
import { HttpClient } from '@angular/common/http';
import { AdVarEditorComponent } from './ad-var-editor.component';
import { SelectionModel } from '@angular/cdk/collections';
import { environment } from '../../environments/environment';

@Component({
  selector: 'app-ad-variants',
  templateUrl: './ad-variants.component.html',
  styleUrls: ['./ad-variants.component.css']
})
export class AdVariantsComponent implements OnInit {
  @ViewChild('filter') filter: ElementRef;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  dataSource: AdVariantsDataSource;
  selection = new SelectionModel<AdVariantsItem>(true, []);

  /** Columns displayed in the table. Columns IDs can be added, removed, or reordered. */
  displayedColumns = ['select', 'name', 'image', 'advertiser', 'adSize', 'clicks.sum', 'selPatterns.selector', 'createdAt'];

  constructor(private http: HttpClient, public dialog: MatDialog) {}

  ngOnInit() {
    this.dataSource = new AdVariantsDataSource(this.filter, this.paginator, this.sort, this.http);
  }

  openDialog(row): void {
    this.dialog.open(AdVarEditorComponent, {
      data: row,
      width: '60%'
    });
  }

  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.items.length;

    return numSelected === numRows;
  }

  masterToggle() {
    this.isAllSelected()
      ? this.selection.clear()
      : this.dataSource.items.forEach(row => this.selection.select(row));
  }

  removeSelected() {
    this.http.delete(`${environment.apiUrl}/boss/ad-variants`, {
      params: {
        ids: this.selection.selected.map(v => v._id).join(',')
      }
    }).subscribe(() => {
      this.filter.nativeElement.dispatchEvent(new FocusEvent('keyup'));
      this.selection.clear();
    });
  }
}
