import { Component, ElementRef, Inject, ViewChild, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { AbstractControl, FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AdVariantsItem } from './ad-variants-datasource';
import { HttpClient } from '../../../node_modules/@angular/common/http';
import { environment } from '../../environments/environment';
import * as _ from 'lodash';

@Component({
  selector: 'app-ad-var-editor',
  templateUrl: './ad-var-editor.component.html',
  styleUrls: ['./ad-var-editor.component.css']
})
export class AdVarEditorComponent implements OnInit {
  @ViewChild('bgImage') bgImage: ElementRef;
  @ViewChild('clicksImage') clicksImage: ElementRef;
  advarForm: FormGroup;
  drawerWidth: Number;

  constructor(
    public dialogRef: MatDialogRef<AdVarEditorComponent>,
    @Inject(MAT_DIALOG_DATA) public data: AdVariantsItem,
    private fb: FormBuilder,
    private http: HttpClient
  ) {
    const gr: any = {};

    gr._id = data._id;
    gr.name = [data.name, [Validators.required, Validators.pattern('^[a-zA-Z0-9_]+$')]];
    gr.advertiser = [data.advertiser, Validators.required];
    gr.selPatternsTxt = [data.selPatternsTxt, [Validators.required, this.validateSelectors]];
    gr.showClicks = [{
      value: data.clicks.sum,
      disabled: !data.clicks.sum
    }];
    this.advarForm = fb.group(gr);
  }

  ngOnInit() {
    this.expandClicksImage();
  }

  validateSelectors(control: AbstractControl): { [key: string]: any; } {
    try {
      const selectors = JSON.parse(control.value);
      let i = 0;

      if (!Array.isArray(selectors)) {
        throw new Error('selPatterns is not array');
      }
      selectors.forEach(s => i += s.length);
      if (i < 1) {
        throw new Error('At least one selector must have length > 0');
      }
    } catch (e) {
      return { validSelectors: e.message };
    }
    return null;
  }

  /* calcClicks() {
    if (!this.data.images.clicks) {
      return;
    }

    const bgImage = this.bgImage.nativeElement;
    const canvas = this.clicksImage.nativeElement;
    const showClicksCtrl = this.advarForm.controls.showClicks;
    const context = canvas.getContext('2d');
    const image = new Image();
    const colors = [];

    canvas.height = bgImage.height;
    canvas.width = bgImage.width;
    image.onload = () => {
      context.drawImage(image, 0, 0, canvas.width, canvas.height);

      for (let x = 0; x < canvas.width; x++) {
        for (let y = 0; y < canvas.height; y++) {
          const p = context.getImageData(x, y, 1, 1).data;

          if (p[3]) {
            colors.push(x + ':' + y);
            context.fillStyle = 'black';
            context.fillRect(x, y, 1, 1);
          }
        }
      }
      this.data.clicks.positions = colors.join(',');
      this.data.clicks.sum = colors.length;
      showClicksCtrl.setValue(true);
      showClicksCtrl.enable();
    };
    image.src = this.data.images.clicks;
  }*/

  expandClicksImage(width = null) {
    if (!this.data.clicks.sum) {
      return;
    }

    const origBgImage = document.createElement('img');
    const bgImage = this.bgImage.nativeElement;
    const canvas = this.clicksImage.nativeElement;
    const context = canvas.getContext('2d');

    origBgImage.onload = () => {
      const xProp = (width || bgImage.width) / origBgImage.width;
      const yProp = bgImage.height / origBgImage.height;

      canvas.width = width || bgImage.width;
      canvas.height = bgImage.height;
      this.data.clicks.positions.split(',').forEach((xy) => {
        const [x, y]: any = xy.split(':');
        const newX = Math.round(x * xProp);

        context.fillStyle = 'black';
        context.fillRect(_.sample([newX - 1, newX, newX + 1]), Math.round(y * yProp), 1, 1);
      });
    };
    origBgImage.src = this.data.image;
  }

  onSubmit() {
    const model = this.advarForm.value;

    this.data.selPatternsTxt = model.selPatternsTxt;
    this.http.post(`${environment.apiUrl}/boss/ad-variant/${model._id}`, {
      name: this.data.name = model.name,
      advertiser: this.data.advertiser = model.advertiser,
      selPatterns: this.data.selPatterns,
      clicks: {
        sum: this.data.clicks.sum,
        positions: this.data.clicks.positions
      }
    }, { responseType: 'text' }).subscribe(() => {
      this.dialogRef.close();
    });
  }

  prepareSelPatterns() {
    if (!this.data.selPatterns.length) {
      this.data.selPatterns = [{
        selector: '',
        length: 1,
        comment: ''
      }];
      this.advarForm.controls.selPatternsTxt.setValue(this.data.selPatternsTxt);
    }
  }

  // genIframe() {
    // const iframeDoc = this.iframe.nativeElement.contentWindow.document;
    // iframeDoc.open();
    // iframeDoc.write(data.code);
    // iframeDoc.close();
  // }
}
