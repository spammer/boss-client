import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AdUnitsComponent } from './ad-units/ad-units.component';
import { AdVariantsComponent } from './ad-variants/ad-variants.component';
import { AdVariantHtmlComponent } from './ad-variant-html/ad-variant-html.component';
import { HistoryComponent } from './history/history.component.';

const routes: Routes = [
  { path: '', redirectTo: '/ad-variants', pathMatch: 'full' },
  { path: 'ad-units', component: AdUnitsComponent },
  { path: 'ad-variants', component: AdVariantsComponent },
  { path: 'ad-html', component: AdVariantHtmlComponent },
  { path: 'history', component: HistoryComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
