
import { fakeAsync, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdUnitsComponent } from './ad-units.component';

describe('AdUnitsComponent', () => {
  let component: AdUnitsComponent;
  let fixture: ComponentFixture<AdUnitsComponent>;

  beforeEach(fakeAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ AdUnitsComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(AdUnitsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should compile', () => {
    expect(component).toBeTruthy();
  });
});
