import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MainNavComponent } from './main-nav/main-nav.component';
import { LayoutModule } from '@angular/cdk/layout';
import {
  MatToolbarModule,
  MatButtonModule,
  MatSidenavModule,
  MatIconModule,
  MatListModule,
  MatTableModule,
  MatPaginatorModule,
  MatSortModule,
  MatProgressSpinnerModule,
  MatDialogModule,
  MatFormFieldModule,
  MatInputModule,
  MatCheckboxModule
} from '@angular/material';
import { AdUnitsComponent } from './ad-units/ad-units.component';
import { AdVariantsComponent } from './ad-variants/ad-variants.component';
import { HttpClientModule } from '@angular/common/http';
import { AdVarEditorComponent } from './ad-variants/ad-var-editor.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { HistoryComponent } from './history/history.component.';
import { AdVariantHtmlComponent } from './ad-variant-html/ad-variant-html.component';
import { HighlightModule } from 'ngx-highlightjs';
import xml from 'highlight.js/lib/languages/xml';

export function highlightLangs() {
  return [{ name: 'xml', func: xml }];
}

@NgModule({
  declarations: [
    MainNavComponent,
    AdUnitsComponent,
    AdVariantsComponent,
    AdVarEditorComponent,
    HistoryComponent,
    AdVariantHtmlComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    HighlightModule.forRoot({
      // TODO: why it doesn't parse at all
      languages: highlightLangs
    }),
    HttpClientModule,
    LayoutModule,
    MatToolbarModule,
    MatButtonModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule,
    MatDialogModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    MatProgressSpinnerModule,
    MatFormFieldModule,
    MatInputModule,
    ReactiveFormsModule,
    MatCheckboxModule,
    FormsModule
  ],
  entryComponents: [AdVarEditorComponent],
  providers: [],
  bootstrap: [MainNavComponent]
})
export class AppModule { }
