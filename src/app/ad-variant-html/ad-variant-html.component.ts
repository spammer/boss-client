import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';

@Component({
  selector: 'app-ad-variant-html',
  templateUrl: './ad-variant-html.component.html',
  styleUrls: ['./ad-variant-html.component.css']
})
export class AdVariantHtmlComponent implements OnInit {
  code = '';

  constructor(
    private http: HttpClient,
    private route: ActivatedRoute) { }

  ngOnInit() {
    this.route.queryParams.subscribe(params => this.http
      .get(`${environment.apiUrl}/boss/ad-variant-html`, {
        params,
        responseType: 'text'
      }).subscribe(code => this.code = code));
  }

}
