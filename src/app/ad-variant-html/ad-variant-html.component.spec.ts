import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdVariantHtmlComponent } from './ad-variant-html.component';

describe('AdVariantHtmlComponent', () => {
  let component: AdVariantHtmlComponent;
  let fixture: ComponentFixture<AdVariantHtmlComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdVariantHtmlComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdVariantHtmlComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
